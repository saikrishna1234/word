### Word Master

- Technologies-used: Html, css, JavaScript
  You have two APIs to work with today:
- GET https://words.dev-apis.com/word-of-the-day
- This will give you the word of the day. It changes every night at midnight
- The response will look like this: {"word":"humph","puzzleNumber":3} where the word is the current word of the day and the puzzleNumber.
- If you add random=1 to the end of your URL (words.dev-apis.com/wordof-the-day/get-word-of-the-day?random=1) then it will give you a random word of the day, not just the same daily one.
- If you add puzzle=<number> to the end of your URL (words.dev-apis.com/wordof-the-day/get-word-of-the-day?puzzle=1337) then it will give you the same word every time.

<br>

<div>
<img src="https://res.cloudinary.com/dcsy19n4b/image/upload/v1677487547/Screenshot_from_2023-02-27_14-13-36_xukjoz.png"/>
</div>

## Try Out Here:

https://bucolic-semifreddo-bc1042.netlify.app/
